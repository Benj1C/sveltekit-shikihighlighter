# SvelteKit + Shiki (Highlighter)


## src/lib/server/shiki.ts
```ts
import shiki from 'shiki';

export const shikiHighlighter = async (code: string, lang = 'text') => {
    const highlighter = await shiki.getHighlighter({
        theme: 'github-light'
    })
    const html = highlighter.codeToHtml(code, { lang })
    return html
}
```

## src/routes/+page.server.ts

```ts
import { shikiHighlighter } from "$lib/server/shiki";
import dartExample from '../snippets/example.dart?raw';

export const load = () => {
    return {
        dartExample: shikiHighlighter(dartExample, 'dart')
    }
}
```

## src/routes/+page.svelte

```ts
<script lang='ts'>
	import type { PageServerData } from './$types';

	export let data: PageServerData;
</script>
```

```html
<div>{@html data.dartExample}</div>
```