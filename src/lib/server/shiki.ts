import shiki from 'shiki';

export const shikiHighlighter = async (code: string, lang = 'text') => {
    const highlighter = await shiki.getHighlighter({
        theme: 'github-light'
    })
    const html = highlighter.codeToHtml(code, { lang })
    return html
}