import 'package:flutter/material.dart';
import 'package:animator/animator.dart';

class Green extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        title: Text('Animation Zoom'),
        iconTheme:
          IconThemeData(color: Colors.white, opacity: 100, size: 30),
      ),
      body: Animation(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.greenAccent,
        child: Icon(Icons.replay_circle_filled),
        tooltip: 'return',
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

class Animation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Animator<double>(
      tween: Tween<double>(begin: 0, end: 1920),
      cycles: 2,
      repeats: 1,
      duration: Duration(milliseconds: 6000),
      builder: (context, animatorState, child) => Center(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: animatorState.value,
          width: animatorState.value,
          child: Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: Image.asset(
              'images/flutter.png',
              fit: BoxFit.cover,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 5,
            margin: EdgeInsets.all(10),
          ),
        )
      ),
    );
  }
}