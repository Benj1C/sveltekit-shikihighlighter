import { shikiHighlighter } from "$lib/server/shiki";
import dartExample from '../snippets/example.dart?raw';

export const load = () => {
    return {
        dartExample: shikiHighlighter(dartExample, 'dart')
    }
}